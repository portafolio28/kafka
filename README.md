# kafka

Este es un servicio de procesos asíncronos que se levanta mediante imágenes y contenedores de docker de licencia gratuita. Este kafka está configurado especialmente para uso masivo garantizando una partición en diferentes nodos y replicación de los tópicos. Utilizado para el aplicativo de pensiones de Bolivia.

Por motivos de privacidad este contenido está estrechamente limitado y se le deshabilito la ejecución correcta en local para evitar plagios o suplantación.
